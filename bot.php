<?php

include('vendor/autoload.php');
include('dom.php');

use Telegram\Bot\Api;

$telegram = new Api('642128197:AAHRfFckcHPHlmwdkznSGOvsskbilAxcF3I');

foreach ($new as $key => $value) {

	$text = $value['name']."\nЦена: ".$value['unitPrice']." тг\nДата обновления: ". $value['lastUpdate']."\nДешевле на ".percentages($value['unitPrice'], $value['attributes']['avgPrice'])." %\n\n";

	if ($value['unitPrice'] < $value['attributes']['avgPrice']) {

		$conn = mysqli_connect($servername, $username, $password, $dbname);
		// Check connection
		if (!$conn) {
		    die("Connection failed: " . mysqli_connect_error());
		}

		$sql = "SELECT last_update FROM cars WHERE cars_id = {$value['id']}";
		$result = mysqli_query($conn, $sql);
		//var_dump($result);
		if (mysqli_num_rows($result) > 0) {
		    // output data of each row
		    $row = mysqli_fetch_assoc($result);
		    //echo $row['last_update'];
		    if (strtotime($row['last_update']) < strtotime($value['lastUpdate'])) {
		    	$sql = "UPDATE cars SET last_update='{$value['lastUpdate']}' WHERE cars_id = {$value['id']}";
		    	if ($conn->query($sql) === TRUE) {
		    		$response = $telegram->sendMessage([
					  'chat_id' => '138267623', 
					  'text' => $text
					]);
				    echo "Record updated successfully";
				} else {
				    echo "Error updating record: " . $conn->error;
				}
		    }
		}

		if (mysqli_num_rows($result) == 0) {
			$sql = "INSERT INTO cars (name, cars_id, last_update)
     		VALUES ('{$value['name']}', '{$value['id']}', '{$value['lastUpdate']}')";
     		if (mysqli_query($conn, $sql)) {
     			$response = $telegram->sendMessage([
				  'chat_id' => '138267623', 
				  'text' => $text
				]);
			    echo "New record created successfully";
			} else {
			    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
			}
		}

		mysqli_close($conn);

	}
}
