<?php

function getPage($url) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
	$out = curl_exec($ch);
	curl_close($ch);
	return $out;
}

function getCars($out) {
    $new = [];
	$regex = "/listing.items.push(.*)/ ";
	if (preg_match_all($regex, $out, $matches)){
		foreach ($matches[1] as $key => $value) {
		    $car = trim(rtrim($value, ";"), "()");
		    $new[] = json_decode($car, true);
	    }
	}
	return $new;
}

function numPages($out) {
	$regex = "#data-nb-adverts-total=[\'|\"](.*)?[\'|\"]#";
	$pages = 1;
	if (preg_match_all($regex, $out, $matches)) {
	  $adt = $matches[1][0];
	  $pages = ceil($adt / 20);
	}
	return $pages;
}

function percentages($unitPrice, $avgPrice) {
	if ($avgPrice != 0) {
		$percentages = (($avgPrice - $unitPrice) / $avgPrice ) * 100;
		return round($percentages, 2);
	}	
}

function getLastUpdateById($id) {
	global $conn;
	$sql = "SELECT last_update FROM cars WHERE cars_id = {$id}";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$row = mysqli_fetch_assoc($result);
		return $row;
	}	
}

function updateLastUpdateById($lastUpdate, $id) {
	global $conn;
	$sql = "UPDATE cars SET last_update='{$lastUpdate}' WHERE cars_id = {$id}";
	if ($conn->query($sql) === TRUE) {
	    return TRUE;
	} else {
		$error = "Error updating record: " . $conn->error;
	    return $error;
	}
}

function createCar($name, $id, $lastUpdate) {
	global $conn;
	$sql = "INSERT INTO cars (name, cars_id, last_update) VALUES ('{$name}', '{$id}', '{$lastUpdate}')";
	if (mysqli_query($conn, $sql)) {
	    return TRUE;
	} else {
		$error = "Error: " . $sql . "<br>" . mysqli_error($conn);
	    return $error;
	}
}

function createFilterPrice($minPrice, $maxPrice) {
	global $conn;
	$sql = "INSERT INTO filter_price (min_price, max_price) VALUES ('{$minPrice}', '{$maxPrice}')";
	if (mysqli_query($conn, $sql)) {
	    return TRUE;
	} else {
		$error = "Error: " . $sql . "<br>" . mysqli_error($conn);
	    return $error;
	}
}

function updateFilterPriceById($minPrice, $maxPrice) {
	global $conn;
	$sql = "UPDATE filter_price SET min_price='{$minPrice}', max_price='{$maxPrice}'";
	if ($conn->query($sql) === TRUE) {
	    return TRUE;
	} else {
		$error = "Error updating record: " . $conn->error;
	    return $error;
	}
}

function getFilterPrice() {
	global $conn;
	$sql = "SELECT * FROM filter_price";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$row = mysqli_fetch_assoc($result);
		return $row;
	}	
}

function getPriceFromMsg($text) {
	$regex = "/price [0-9]+ [0-9]+/ ";
	if (preg_match_all($regex, $text, $matches)) {
		$price = explode(" ", $matches[0][0]);
		$prices['minPrice'] = $price[1];
		$prices['maxPrice'] = $price[2];
		return $prices;
	}
}

// Возвращает год выпуска машины
function carYear($car) {
    preg_match("#[0-9]{4} год#", $car, $match);
    $arr = explode(' ', $match[0]);
    $year = $arr[0];
    return $year;
}

// Текст сообщения
function text($brand, $model, $name, $unitPrice, $avgPrice, $url, $lastUpdate){

    $carName = "$brand $model";
    $year = carYear($name);
    $price = $unitPrice;
    $avgPrice = $avgPrice;
    $percents = percentages($unitPrice, $avgPrice);
    $a = $url;
    $date = date("h:i d.m.Y", strtotime($lastUpdate));

    $text = "
<b>Автомобиль:</b> $carName
<b>Год:</b> $year г.
<b>Цена:</b> $price т
<b>Дешевле:</b> на $percents%
<b>Средняя цена по рынку:</b> $avgPrice т
<b>Дата последнего обновления объявления:</b> $date
<b>Ссылка на объявление:</b> <a href=\"$a\">$a</a>";

return $text;

}

// $str = "price 20000 500000";
// $regex = "/price(.*)/ ";
// preg_match_all($regex, $str, $matches);
// $price = explode(" ", $matches[0][0]);
// $minPrice = $price[1];
// $maxPrice = $price[2];
// var_dump($maxPrice);


