<?php

// Ссылка для установки вебхука (вставить ее в браузере)
// команда для запуска ngrok 'ngrok http 80 --host-header=bot.loc'
$whA = 'https://api.telegram.org/bot642128197:AAHRfFckcHPHlmwdkznSGOvsskbilAxcF3I/setWebhook?url=https://92599684.ngrok.io/main.php';

include('db.php');
include('vendor/autoload.php');
include('func.php');

use Telegram\Bot\Api;
$telegram = new Api('642128197:AAHRfFckcHPHlmwdkznSGOvsskbilAxcF3I');

$price = getFilterPrice();

$baseUrl = 'https://kolesa.kz/cars/?region=ekibastuz,pavlodar&auto-custom=2&price[from]='.$price['min_price'].'&price[to]='.$price['max_price'];

$page = '&page=';
$html = getPage($baseUrl);
$pages = numPages($html);

for ($i=1; $i <= 2; $i++) {

	$html = getPage($baseUrl.$page.$i);
	$cars = getCars($html);

	foreach ($cars as $value) {
		
		if (isset($value['attributes']['avgPrice']) && ($value['unitPrice'] < $value['attributes']['avgPrice'])) {
		    //$percents = percentages($value['unitPrice'], $value['attributes']['avgPrice']);
		    //$text = $value['name']."\nЦена: ".$value['unitPrice']." тг\nДата обновления: ". $value['lastUpdate']."\nДешевле на $percents%\n\n";
		    $text = text($value['attributes']['brand'], $value['attributes']['model'], $value['name'], $value['unitPrice'], $value['attributes']['avgPrice'], $value['url'], $value['lastUpdate']);
			$row = getLastUpdateById($value['id']);
			if ($row && (strtotime($row['last_update']) < strtotime($value['lastUpdate']))) {
				if (updateLastUpdateById($value['lastUpdate'], $value['id'])) {
					$response = $telegram->sendMessage([
					  'chat_id' => '138267623', 
					  'text' => $text,
					  'parse_mode' => 'HTML'
					]);
					
				}
			}

			if ($row == FALSE) {
				if (createCar($value['name'], $value['id'], $value['lastUpdate'])) {
					$response = $telegram->sendMessage([
					  'chat_id' => '138267623', 
					  'text' => $text,
					  'parse_mode' => 'HTML'
					]);
				}
			}
		}
	}

}